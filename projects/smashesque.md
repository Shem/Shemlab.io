---
title: Smashesque (website)
images:
    - ../images/smashesque.png
---

Smashesque is a simple website that lists platform fighting games similar to, but not including, the Super Smash Brothers series.

[Visit the website here!](https://www.smashesque.com)

